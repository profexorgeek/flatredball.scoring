using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace FlatRedBall.Scoring.Android
{
    /// <summary>
    /// An interface for a configuration file.
    /// This mostly exists so I don't have to check 
    /// a config with secret API keys into source control 
    /// but other developers can see how to structure a config.
    /// 
    /// These values should all come from the Google Developer Console
    /// </summary>
    public interface IConfig
    {
        string ClientId { get;}
        string LeaderboardId { get;}
        string ClientSecret { get;}
    }
}