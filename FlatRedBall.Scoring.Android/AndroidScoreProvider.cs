using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.Gms.Games;
using Android.Gms.Games.LeaderBoard;
using Leaderboards = Android.Gms;
using Android.Content;

namespace FlatRedBall.Scoring.Android
{
	public class AndroidScoreProvider : IPlatformScoreProvider
    {
        private IConfig config;
        private Context context;
        private IGoogleApiClient apiClient;
		private ConnectionStatus connection;

        /// <summary>
        /// Initializes the configuration file and sets up save file
        /// </summary>
        public AndroidScoreProvider(Context contextReference)
        {
            context = contextReference;
			connection = new ConnectionStatus ();

            // This is not included in the repository because it includes private key information.
            // Build your own config file with your API key data and init here.
            config = new DebugGoogleApiConfig();

            GoogleApiClientBuilder builder = new GoogleApiClientBuilder(contextReference);
            builder.AddApi(GamesClass.API);
            builder.AddScope(GamesClass.ScopeGames);
			builder.AddConnectionCallbacks (connection);
			builder.AddOnConnectionFailedListener (connection);

			try {
            	apiClient = builder.Build();
			}
			catch(Exception e) {
				throw e;
			}

			ConnectionResult result = apiClient.BlockingConnect ();
			if (!result.IsSuccess)
			{
				int error = result.ErrorCode;
				throw new Exception ("Failed to connect!");
			}
        }

		#region IPlatformScoreProvider implementation
		public List<PlayerScoreModel> LocalScores
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public string Username
		{
			get
			{
				throw new NotImplementedException();
			}
		}

        public void Authenticate()
        {
            throw new NotImplementedException();
        }

        public void ClearLocalScores()
        {
            throw new NotImplementedException();
        }

        public List<PlayerScoreModel> GetLeaderboardScores(int startingRecord, int limit)
        {
            throw new NotImplementedException();
        }

        public void SubmitLeaderboardScore(PlayerScoreModel score)
        {
            throw new NotImplementedException();
        }

        public void SubmitLocalScore(PlayerScoreModel score)
        {
            throw new NotImplementedException();
        }
		#endregion


    }
}
