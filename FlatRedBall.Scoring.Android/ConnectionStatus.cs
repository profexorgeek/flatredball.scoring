﻿using System;
using Android.Gms.Common.Apis;
using Android.Gms.Common;

namespace FlatRedBall.Scoring.Android
{
	internal class ConnectionStatus : Java.Lang.Object, IGoogleApiClientConnectionCallbacks, IGoogleApiClientOnConnectionFailedListener
	{
		public bool Connected { get; set; }

		public int ConnectionResultCode { get; private set; }

		public ConnectionStatus()
		{
			Connected = false;
		}

		#region IGoogleApiClientOnConnectionFailedListener
		public void OnConnectionFailed (ConnectionResult result)
		{
			ConnectionResultCode = result.ErrorCode;
			Connected = false;
		}
		#endregion

		#region IGoogleApiClientConnectionCallbacks implementation
		public void OnConnected (global::Android.OS.Bundle connectionHint)
		{
			Connected = true;
		}

		public void OnConnectionSuspended (int cause)
		{
			Connected = false;
		}
		#endregion
	}
}

