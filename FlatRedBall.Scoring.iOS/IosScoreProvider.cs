﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlatRedBall.Scoring.iOS
{
    public class IosScoreProvider : IPlatformScoreProvider
    {
        public List<PlayerScoreModel> LocalScores
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string Username
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Authenticate()
        {
            throw new NotImplementedException();
        }

        public void ClearLocalScores()
        {
            throw new NotImplementedException();
        }

        public List<PlayerScoreModel> GetLeaderboardScores(int startingRecord, int limit)
        {
            throw new NotImplementedException();
        }

        public void SubmitLeaderboardScore(PlayerScoreModel score)
        {
            throw new NotImplementedException();
        }

        public void SubmitLocalScore(PlayerScoreModel score)
        {
            throw new NotImplementedException();
        }
    }
}
