﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FlatRedBall.Scoring.Windows
{
    public class WindowsScoreProvider : IPlatformScoreProvider
    {
        private string savePath;
        private List<PlayerScoreModel> localScores;

        /// <summary>
        /// Constructor optionally takes a save file name
        /// </summary>
        /// <param name="saveFileName">The name of the file to create to store scores</param>
        public WindowsScoreProvider(string saveFileName = "leaderboard.score")
        {
            savePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), saveFileName);
            localScores = LoadLocalScores();
        }

        /// <summary>
        /// The logged in windows user name
        /// </summary>
        public string Username
        {
            get
            {
                return Environment.UserName;
            }
        }

        /// <summary>
        /// The list of locally-saved scores
        /// </summary>
        public List<PlayerScoreModel> LocalScores
        {
            get
            {
                return localScores;
            }
        }

        /// <summary>
        /// This windows provider does not require authentication because it doesn't provide a leaderboard service
        /// </summary>
        public void Authenticate()
        {
            // Intentionally does nothing, debug version implements no auth
        }

        /// <summary>
        /// Deletes all locally-saved scores
        /// </summary>
        public void ClearLocalScores()
        {
            localScores = new List<PlayerScoreModel>();
            SaveLocalScores();
        }

        /// <summary>
        /// Since this does not implement a leaderboard, this simply returns the local scores.
        /// </summary>
        /// <param name="startingRecord">Unused</param>
        /// <param name="limit">Unused</param>
        /// <returns>The whole list of local scores</returns>
        public List<PlayerScoreModel> GetLeaderboardScores(int startingRecord, int limit)
        {
            return localScores;
        }

        /// <summary>
        /// This provider doesn't implement a leaderboard so this method intentionally does nothing.
        /// </summary>
        /// <param name="score">Unused</param>
        public void SubmitLeaderboardScore(PlayerScoreModel score)
        {
            // Intentionally does nothing, debug version implements no service
        }

        /// <summary>
        /// Saves a new score to the local scores.
        /// </summary>
        /// <param name="score">The score object to save</param>
        public void SubmitLocalScore(PlayerScoreModel score)
        {
            localScores.Add(score);
            SaveLocalScores();
        }

        /// <summary>
        /// Saves all scores in the localScores object to an XML file
        /// </summary>
        private void SaveLocalScores()
        {
            using (TextWriter stream = new StreamWriter(savePath))
            {
                var serializer = new XmlSerializer(localScores.GetType());
                serializer.Serialize(stream, localScores);
            }

        }

        /// <summary>
        /// Loads and returns scores from an XML file
        /// </summary>
        /// <returns></returns>
        private List<PlayerScoreModel> LoadLocalScores()
        {
            List<PlayerScoreModel> loadedScores = new List<PlayerScoreModel>();
            if (File.Exists(savePath))
            {
                try
                {
                    using (FileStream stream = new FileStream(savePath, FileMode.OpenOrCreate))
                    {
                        var serializer = new XmlSerializer(loadedScores.GetType());
                        loadedScores = (List<PlayerScoreModel>)serializer.Deserialize(stream);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Unable to load saved scores: " + e.Message);
                }
            }

            return loadedScores;
        }
    }
}
