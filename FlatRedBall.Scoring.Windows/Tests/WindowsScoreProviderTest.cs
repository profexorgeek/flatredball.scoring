﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlatRedBall.Scoring.Windows.Tests
{
    /// <summary>
    /// Easy way to make sure score APIs are working without spinning up and playing the game.
    /// Note that you can right-click-->Debug Test to step through if testing an API implementation
    /// </summary>
    [TestClass]
    public class WindowsScoreProviderTest
    {

        /// <summary>
        /// Just makes sure username isn't empty
        /// </summary>
        [TestMethod]
        public void TestUsername()
        {
            ScoreManager.Initialize(new WindowsScoreProvider());

            string username = ScoreManager.Username;
            Assert.AreEqual(string.IsNullOrWhiteSpace(username), false);
        }

        /// <summary>
        /// Tests incrementing, decrementing, clearing and finalizing scores
        /// </summary>
        [TestMethod]
        public void TestAffectScore()
        {
            ScoreManager.Initialize(new WindowsScoreProvider());

            // should reset score to zero
            ScoreManager.DestroyCurrentScore();
            Assert.AreEqual(ScoreManager.CurrentScore, 0);

            // should add 100 to score
            ScoreManager.AffectScore(100);
            Assert.AreEqual(ScoreManager.CurrentScore, 100);

            // should subtract 50 from current score
            ScoreManager.AffectScore(-50);
            Assert.AreEqual(ScoreManager.CurrentScore, 50);

            // wipe score out, should not appear in save list
            ScoreManager.DestroyCurrentScore();
            Assert.AreEqual(ScoreManager.CurrentScore, 0);

            // add 250 to score
            ScoreManager.AffectScore(250);
            Assert.AreEqual(ScoreManager.CurrentScore, 250);

            // should save record and reset score to zero
            // do NOT submit to leaderboard
            ScoreManager.FinalizeScore(false);
            Assert.AreEqual(ScoreManager.CurrentScore, 0);
        }

        /// <summary>
        /// Creates a bunch of scores and makes sure the count
        /// matches the number of scores that should be saved
        /// </summary>
        [TestMethod]
        public void TestSavedScoreLists()
        {
            ScoreManager.Initialize(new WindowsScoreProvider());

            // use a RNG for unique scores
            Random rand = new Random();
            List<int> scores = new List<int>();

            // wipe out all existing scores
            ScoreManager.ClearLocalScores();
            ScoreManager.DestroyCurrentScore();

            // create and finalize some scores
            for (int i = 0; i < 10; i++)
            {
                int iterationScore = rand.Next(0, 100);
                ScoreManager.AffectScore(iterationScore);
                ScoreManager.FinalizeScore(false);
                scores.Add(iterationScore);
            }

            // make sure our count is correct!
            Assert.AreEqual(ScoreManager.LocalScores.Count, 10);
        }
    }
}
