﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlatRedBall.Scoring
{
    /// <summary>
    /// Exposes a platform-agnostic API to track and save player scores.
    /// </summary>
    public static class ScoreManager
    {
        #region Properties
        /// <summary>
        /// A provider for saving local and leaderboard scores, specific to the build platform
        /// </summary>
        private static IPlatformScoreProvider ScoreProvider
        {
            get
            {
                if(scoreProvider == null)
                {
                    throw new Exception("No score provider available! Have you initialized this manager?");
                }
                return scoreProvider;
            }
            set
            {
                scoreProvider = value;
            }
        }

        /// <summary>
        /// The local username, if available from a game or other service. Otherwise could be empty.
        /// </summary>
        public static string Username
        {
            get
            {
                return ScoreProvider.Username;
            }
        }

        /// <summary>
        /// The running score for the game
        /// </summary>
        public static int CurrentScore
        {
            get
            {
                return currentScore;
            }
            private set
            {
                currentScore = value;
            }
        }

        /// <summary>
        /// Returns all local scores
        /// </summary>
        public static List<PlayerScoreModel> LocalScores
        {
            get
            {
                return ScoreProvider.LocalScores;
            }
        }
#endregion

#region Fields
        private static IPlatformScoreProvider scoreProvider;
        private static int currentScore = 0;
        private static bool initialized = false;
#endregion


#region Methods

        /// <summary>
        /// Sets up the manager with a platform-specific score provider.
        /// This must be called before the ScoreManager can be used!
        /// </summary>
        /// <param name="provider">A platform-specific provider</param>
        public static void Initialize(IPlatformScoreProvider provider)
        {
            scoreProvider = provider;
            initialized = true;
        }

        /// <summary>
        /// This should be called as early as possible to make sure player is authenticated
        /// for submitting scores.
        /// </summary>
        public static void Authenticate()
        {
            ScoreManager.Authenticate();
        }

        /// <summary>
        /// Affects the player's score by the provided amount. Note that this can be negative.
        /// </summary>
        /// <param name="amount">An amount to affect the score, can be negative</param>
        public static void AffectScore(int amount)
        {
            CurrentScore += amount;

            if(amount <= 0)
            {
                // TODO: see if anything needed by playing game
            }
        }

        /// <summary>
        /// Saves the current score locally and to a leaderboard, then resets it to zero for the next game
        /// </summary>
        /// <param name="submitToLeaderboard">Set to false to avoid submitting score publicly. Default is true</param>
        public static void FinalizeScore(bool submitToLeaderboard = true)
        {
            PlayerScoreModel scoreModel = new PlayerScoreModel()
            {
                Username = ScoreProvider.Username,
                ScoreValue = CurrentScore,
                Timestamp = DateTime.Now
            };

            ScoreProvider.SubmitLocalScore(scoreModel);

            if(submitToLeaderboard)
            {
                ScoreProvider.SubmitLeaderboardScore(scoreModel);
            }

            CurrentScore = 0;
        }

        /// <summary>
        /// Resets player score to zero without saving anywhere.
        /// </summary>
        public static void DestroyCurrentScore()
        {
            currentScore = 0;
        }

        /// <summary>
        /// Resets all local scores
        /// </summary>
        public static void ClearLocalScores()
        {
            ScoreProvider.ClearLocalScores();
        }
#endregion
    }
}
