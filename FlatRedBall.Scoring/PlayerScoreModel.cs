﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlatRedBall.Scoring
{
    // A data object representing a player score that can be serialized to disk or submitted to a leaderboard API
    public class PlayerScoreModel
    {
        // The player that earned the score
        public string Username { get; set; }

        // A final score for a game
        public int ScoreValue { get; set; }

        // The time the score was achieved
        public DateTime Timestamp { get; set; }
    }
}
