﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlatRedBall.Scoring
{
    // A platform-specific provider for saving scores locally and to an API
    public interface IPlatformScoreProvider
    {
        // should return local player's leaderboard username if available
        string Username { get; }

        // should get locally-saved user scores
        List<PlayerScoreModel> LocalScores { get; }

        // should get a paginated list of scores from a leaderboard service
        List<PlayerScoreModel> GetLeaderboardScores(int startingRecord, int limit);

        // authenticates the player's leaderboard credentials to make score submission easier later
        void Authenticate();

        // should save player score to a leaderboard service
        void SubmitLeaderboardScore(PlayerScoreModel score);

        // should save a player score to local device
        void SubmitLocalScore(PlayerScoreModel score);        

        // should delete all local scores
        void ClearLocalScores();

    }
}
